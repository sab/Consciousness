package systems

import "github.com/shabinesh/shooter/components"

// System acts on Component
type System interface {
	// systems manage entities
	AddComponent(components.Component)
	Start()
	Weight() int
}
