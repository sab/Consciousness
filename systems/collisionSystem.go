package systems

import (
	"github.com/shabinesh/shooter/components"
	"github.com/shabinesh/shooter/entity"
	"github.com/veandco/go-sdl2/sdl"
)

type CollisionDetectionSystem struct {
	components []components.Component
}

func (c *CollisionDetectionSystem) AddComponent(comp components.Component) {
	c.components = append(c.components, comp)
}

func (c *CollisionDetectionSystem) Weight() int {
	return 3
}

func collided(a, b entity.Entity) bool {
	// 	If a's bottom right x coordinate is less than b's top left x coordinate
	//     There is no collision
	// If a's top left x is greater than b's bottom right x
	//     There is no collision
	// If a's top left y is greater than b's bottom right y
	//     There is no collision
	// If a's bottom right y is less than b's top left y
	// There is no collision

	_, ah := a.GetDim()
	bw, bh := b.GetDim()
	ax, ay := a.GetPos()
	bx, by := b.GetPos()

	if ay+ah < by {
		return false
	}
	if ay >= by+bh {
		return false
	}
	if ax >= bx+bw {
		return false
	}
	return true
}

func (c *CollisionDetectionSystem) Start() {
	for {
		for _, comp := range c.components {
			for _, a := range comp.All() {
				for _, b := range comp.All() {
					//fmt.Printf("%+v %d %d\n", *a, (*a).GetID(), (*b).GetID())
					if (*a).GetID() != (*b).GetID() {
						if yes := collided(*a, *b); yes {
							println("collided")
						}
					}
				}
			}
		}
		sdl.Delay(100)
	}
}
