package systems

import (
	"github.com/shabinesh/shooter/components"
	"github.com/shabinesh/shooter/internal"
	"github.com/veandco/go-sdl2/sdl"
)

type RenderSystem struct {
	components []components.Component
}

func (r *RenderSystem) Start() {
	for {
		dt := sdl.GetTicks()
		for _, comp := range r.components {
			es := comp.All()
			for _, e := range es {
				(*e).Update(dt)
			}
		}
		internal.Renderer.Present()
		sdl.Delay(10)
	}
}

func (r *RenderSystem) AddComponent(c components.Component) {
	r.components = append(r.components, c)
}

func (r *RenderSystem) Weight() int {
	return 2
}
