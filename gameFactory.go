package main

import (
	"fmt"
	"log"

	"github.com/shabinesh/shooter/components"
	"github.com/shabinesh/shooter/entity"
	"github.com/shabinesh/shooter/internal"
	"github.com/shabinesh/shooter/systems"
	"github.com/veandco/go-sdl2/sdl"
	img "github.com/veandco/go-sdl2/sdl_image"
)

func gameFactory() *Shooter {
	w := &Shooter{}
	w.Events = make(chan *sdl.Event, 100)

	// create entities
	background := &entity.Background{}
	background.W = int32(internal.WindowWidth)
	background.H = int32(internal.WindowWidth)
	background.X, background.Y = 0, 2048
	t, err := img.LoadTexture(internal.Renderer, "res/background.jpg")
	if err != nil {
		log.Fatal("failed to load background image")
	}
	background.Tex = append(background.Tex, t)

	// Create plane
	plane := &entity.Plane{}
	for i := 1; i < 8; i++ {
		p := fmt.Sprintf("res/plane/%d.png", i)
		t, err := img.LoadTexture(internal.Renderer, p)
		if err != nil {
			log.Fatal("failed to load plane")
		}
		t.Unlock()
		plane.Tex = append(plane.Tex, t)
	}
	plane.X, plane.Y = 100, 350
	plane.W, plane.H = 100, 100

	// create asteriods
	asteroid := &entity.Asteroid{}
	t, err = img.LoadTexture(internal.Renderer, "res/asteroids/asteroid_brown.png")
	if err != nil {
		log.Fatal("failed to load asteroids")
	}
	asteroid.Tex = append(asteroid.Tex, t)
	asteroid.X, asteroid.Y = 0, 0
	asteroid.Dest.X, asteroid.Dest.Y = 100, 360
	asteroid.Speed = 2
	asteroid.W = 30
	asteroid.H = 30
	// Create Components
	scene := &components.Scene{}
	scene.AddEntity(background)
	scene.AddEntity(plane)
	scene.AddEntity(asteroid)
	// Create systems
	renderSys := &systems.RenderSystem{}
	renderSys.AddComponent(scene)
	w.AddSystem(renderSys)
	collisionSys := &systems.CollisionDetectionSystem{}
	collisionSys.AddComponent(scene)
	w.AddSystem(collisionSys)

	return w
}
