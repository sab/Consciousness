package components

import (
	"github.com/shabinesh/shooter/entity"
)

const (
	CommandDelete = iota
)

// Component pieces are worked upon by Systems
type Component interface {
	AddEntity(entity.Entity)
	DeleteEntity(entity.Entity)
	All() []*entity.Entity
}

type Mail struct {
	Id      int
	Command int
}
