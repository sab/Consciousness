package components

import (
	"math/rand"

	"github.com/shabinesh/shooter/entity"
)

type Scene struct {
	e []*entity.Entity
}

func (s *Scene) AddEntity(e entity.Entity) {
	// generate an uniq Id for entity
	e.SetID(rand.Intn(1000))
	s.e = append(s.e, &e)
}

func (s *Scene) DeleteEntity(e entity.Entity) {

}

func (s *Scene) All() []*entity.Entity {
	return s.e
}
