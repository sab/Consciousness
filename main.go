package main

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/shabinesh/shooter/internal"
	"github.com/veandco/go-sdl2/sdl"
	"golang.org/x/mobile/app"
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func main() {
	app.Main(func(a app.App) {
		shooter := gameFactory()
		shooter.Run()

	while:
		for {
			for event := sdl.PollEvent(); event != nil; event = sdl.WaitEvent() {
				switch event.(type) {
				case *sdl.KeyDownEvent:
					//shooter.Events <- &event
					fmt.Print("Hey you pressed a key")
				case *sdl.QuitEvent:
					break while
				}
			}
		}

		internal.Qxit()
	})

}
