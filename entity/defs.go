package entity

import (
	"github.com/veandco/go-sdl2/sdl"
)

// Entity is container of Component
type Entity interface {
	Update(uint32)
	SetID(int)
	GetID() int
	GetDim() (int32, int32)
	GetPos() (int32, int32)
}

type Basic struct {
	ID  int
	Tex []*sdl.Texture
}

type Space struct {
	X, Y int32
}

type Size struct {
	W, H int32
}
