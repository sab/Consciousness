package entity

import (
	"github.com/shabinesh/shooter/internal"
	"github.com/veandco/go-sdl2/sdl"
)

// plane is the container of a fighter jet
type Plane struct {
	Basic
	Space
	Size
	i int
}

func (p *Plane) Update(dt uint32) {
	if p.i >= len(p.Tex)-1 {
		p.i = 0
	} else {
		p.i++
	}

	internal.Renderer.Copy((*p).Tex[p.i], nil,
		&sdl.Rect{X: p.X, Y: p.Y, W: p.W, H: p.H})
}

func (p *Plane) SetID(i int) {
	p.ID = i
}

func (p *Plane) GetID() int {
	return p.ID
}

func (p *Plane) GetDim() (int32, int32) {
	return p.W, p.H
}

func (p *Plane) GetPos() (int32, int32) {
	return p.X, p.Y
}
