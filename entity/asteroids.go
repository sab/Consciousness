package entity

import (
	"math"
	"math/rand"

	"github.com/shabinesh/shooter/internal"
	"github.com/veandco/go-sdl2/sdl"
)

type Asteroid struct {
	Basic
	Space
	Size
	Dest   Space
	Speed  int32
	slope  float64
	lastdt uint32
}

func (a *Asteroid) Update(dt uint32) {
	if a.slope == 0 {
		a.slope = math.Exp(rand.Float64())
		//math.Abs(float64(a.Y-a.Dest.Y) / float64(a.X-a.Dest.X))
	}
	if dt-a.lastdt >= 50 {
		a.lastdt = dt
		a.X += a.Speed
		a.Y = int32((a.slope*float64(a.X))+float64(30)) + 1
		if a.Y > int32(internal.WindowHeight) {
			a.Y, a.X = 0, rand.Int31n(int32(internal.WindowWidth))
		}
	}
	internal.Renderer.Copy(
		a.Tex[0],
		nil,
		&sdl.Rect{X: a.X, Y: a.Y, W: a.W, H: a.H})
}

func (a *Asteroid) SetID(i int) {
	a.ID = i
}

func (a *Asteroid) GetID() int {
	return a.ID
}

func (a *Asteroid) GetDim() (int32, int32) {
	return a.W, a.H
}

func (a *Asteroid) GetPos() (int32, int32) {
	return a.X, a.Y
}
