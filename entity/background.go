package entity

import (
	"github.com/shabinesh/shooter/internal"
	"github.com/veandco/go-sdl2/sdl"
)

type Background struct {
	Basic
	Space
	Size
}

func (b *Background) Update(dt uint32) {
	if b.Y-int32(internal.WindowHeight) <= 0 {
		b.Y = 2048
	} else {
		b.Y -= 2
	}
	internal.Renderer.Copy(
		b.Tex[0],
		&sdl.Rect{X: b.X, Y: b.Y, W: 320, H: 480},
		&sdl.Rect{X: 0, Y: 0, W: 320, H: 480})
}

func (b *Background) SetID(i int) {
	b.ID = i
}

func (b *Background) GetID() int {

	return b.ID
}

func (b *Background) GetDim() (int32, int32) {
	return b.W, b.H
}

func (b *Background) GetPos() (int32, int32) {
	return b.X, b.Y
}
