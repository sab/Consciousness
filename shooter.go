package main

import (
	"sort"

	"github.com/shabinesh/shooter/systems"
	"github.com/veandco/go-sdl2/sdl"
)

/*
The world is full of Systems,
Systems manage entities
entity has components
*/

// World of game
type World interface {
	// The world of everything systems
	AddSystem(*systems.System)
	Update()
}

// Shooter implements sort and World
type Shooter struct {
	Events  chan *sdl.Event
	systems []*systems.System
	dt      uint32
}

func (w *Shooter) AddSystem(s systems.System) {
	w.systems = append(w.systems, &s)
	sort.Sort(w) // sort systems by weight
}

func (w Shooter) Run() {
	for _, s := range w.systems {
		go (*s).Start()
	}
}

func (w Shooter) Swap(i, j int) {
	w.systems[i], w.systems[j] = w.systems[j], w.systems[i]
}

func (w Shooter) Len() int {
	return len(w.systems)
}

func (w Shooter) Less(i, j int) bool {
	return (*w.systems[i]).Weight() < (*w.systems[j]).Weight()
}
