package internal

import (
	"log"

	"github.com/veandco/go-sdl2/sdl"
)

var (
	Window       *sdl.Window
	Renderer     *sdl.Renderer
	WindowWidth  = 320
	WindowHeight = 480
)

func init() {
	/* create a window */
	sdl.Init(sdl.INIT_EVERYTHING)
	Window, err := sdl.CreateWindow(
		"Shooter",
		sdl.WINDOWPOS_CENTERED,
		sdl.WINDOWPOS_CENTERED,
		WindowWidth,
		WindowHeight,
		sdl.WINDOW_SHOWN)

	if err != nil {
		log.Fatal("failed to create window")
	}

	Renderer, err = sdl.CreateRenderer(Window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		log.Fatal(err)
	}
}

func Qxit() {
	Renderer.Destroy()
	Window.Destroy()
}
